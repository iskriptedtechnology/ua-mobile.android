import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs/Observable';
import { App } from 'ionic-angular'
import 'rxjs/add/operator/do';;

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

    constructor(
        public auth: AuthService,
        public app: App
    ) {

    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        let nav = this.app.getActiveNav();

        if (request.url.indexOf('upload') !== -1) {

            request = request.clone({
                setHeaders: {
                    Authorization: `Bearer ${this.auth.getToken()}`
                }
            });

        } else {

            request = request.clone({
                setHeaders: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    Authorization: `Bearer ${this.auth.getToken()}`
                }
            });
        }

        return next.handle(request).do((event: HttpEvent<any>) => {

            if (event instanceof HttpResponse) {

                // console.log(event.body);
            }

        }, (err: any) => {

            if (err instanceof HttpErrorResponse) {

                if (err.status === 401) {

                    localStorage.clear();
                    // this.navCtrl.setRoot('LoginPage');
                    nav.setRoot('LoginPage');

                } else if (err.status === 500) {

                    if (err.error.message == 'Token has expired') {

                        localStorage.clear();
                        // this.navCtrl.setRoot('LoginPage');
                        nav.setRoot('LoginPage');
                    }

                } else if (err.status === 400) {

                    if (err.error.error === 'token_not_provided') {

                        // this.navCtrl.setRoot('LoginPage');
                        nav.setRoot('LoginPage');
                    }
                }
            }
        });
    }
}
