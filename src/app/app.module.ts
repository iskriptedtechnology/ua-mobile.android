import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { MyApp } from './app.component';
import { CoreModule } from '../core/core.module';
import { LoginService } from '../pages/login/login.service';
import { AppService } from './app.service';
import { AnnouncementService } from '../pages/announcement/announcement.service';
import { NewsService } from '../pages/news/news.service';
import { EventService } from '../pages/event/event.service';
import { UserService } from '../pages/user/user.service';
import { SettingService } from '../pages/settings/settings.service';

@NgModule({
    declarations: [
        MyApp
    ],
    imports: [
        CoreModule,
        IonicModule.forRoot(MyApp)
    ],
    bootstrap: [
        IonicApp
    ],
    entryComponents: [
        MyApp
    ],
    providers: [
        StatusBar,
        SplashScreen,
        {
            provide: ErrorHandler, 
            useClass: IonicErrorHandler
        },
        LoginService,
        AppService,
        AnnouncementService,
        NewsService,
        EventService,
        UserService,
        SettingService
    ]
})
export class AppModule {}
