import { Injectable } from '@angular/core';
import { Config } from './app.config';

@Injectable()
export class AppService {

    public API_ENDPOINT = Config.API_ENDPOINT;

    constructor() { }
}