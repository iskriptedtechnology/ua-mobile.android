import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController, LoadingController } from 'ionic-angular';
import { UserService } from '../user/user.service';
import { Subscription } from 'rxjs/Subscription';

/**
 * Generated class for the UserChangePasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-user-change-password',
    templateUrl: 'user-change-password.html',
})
export class UserChangePasswordPage {

    old_password: string;
    password: string;
    password_confirmation: string;

    private _userSubscription: Subscription;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public viewCtrl: ViewController,
        public alertCtrl: AlertController,
        public loadingCtrl: LoadingController,
        private _userService: UserService
    ) {
    }
  
    ionViewDidLoad() {
        console.log('ionViewDidLoad UserChangePasswordPage');
    }

    ionViewWillLeave() {

        if (this._userSubscription) {
            this._userSubscription.unsubscribe();
        }
    }

    create(): void {

        if (!this.old_password) {
            this.alertCtrl.create({
                title: 'Field Required',
                subTitle: 'Old Password is required! Please do not leave it blank.',
                buttons: ['Okay']
            })
            .present();
            return;
        }
        if (!this.password) {
            this.alertCtrl.create({
                title: 'Field Required',
                subTitle: 'New Password is required! Please do not leave it blank.',
                buttons: ['Okay']
            })
            .present();
            return;
        }
        if (this.password != this.password_confirmation) {
            this.alertCtrl.create({
                title: 'Field Required',
                subTitle: 'Incorrect Password Confirmation! Please do not leave it blank.',
                buttons: ['Okay']
            })
            .present();
            return;
        }

        let loading = this.loadingCtrl.create({
            content: 'Updating Password...',
        });

        loading.present();

        this._userSubscription = this._userService.changePassword(this.old_password, this.password, this.password_confirmation).subscribe((response: any) => {

            loading.dismiss();

            if (!response || (response && !response.status)) {

                this.alertCtrl.create({
                    title: 'User',
                    subTitle: response.message,
                    buttons: ['Okay']
                })
                .present();

                return;
            }

            this.alertCtrl.create({
                title: 'Password Changed',
                subTitle: 'Password changed successfully!',
                buttons: [{
                    text: 'Okay',
                    handler: () => {
                        this.viewCtrl.dismiss()
                    }
                }]
            })
            .present();

            this.old_password = '';
            this.password = '';
            this.password_confirmation = '';
        });
    }

    close(): void {

        this.viewCtrl.dismiss();
    }

}
