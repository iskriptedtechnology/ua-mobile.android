import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-home',
    templateUrl: 'home.html',
})
export class HomePage {

    public announcementTab: string = 'AnnouncementPage';
    public newsTab: string = 'NewsPage';
    public eventTab: string = 'EventPage';
    public userTab: string = 'UserPage';
    public settingsTab: string = 'SettingsPage';

    constructor(
        public navCtrl: NavController, 
        public navParams: NavParams,
        public alertCtrl: AlertController
    ) {
    }
  
    ionViewDidLoad() {

        console.log('ionViewDidLoad HomePage');

        setTimeout(() => {
            
            if (!localStorage.getItem('tkn')) {

                this.navCtrl.setRoot('LoginPage');
            }
        }, 100);
    }

}
