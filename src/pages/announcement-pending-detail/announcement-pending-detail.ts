import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, ViewController, App, ModalController } from 'ionic-angular';
import { AnnouncementService } from '../announcement/announcement.service';
import { Subscription } from 'rxjs/Subscription';

/**
 * Generated class for the AnnouncementPendingDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-announcement-pending-detail',
    templateUrl: 'announcement-pending-detail.html',
})
export class AnnouncementPendingDetailPage {

    announcement: any;
    is_owner: boolean;
    is_admin: boolean;

    private _announcementSubscription: Subscription;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public alertCtrl: AlertController,
        public loadingCtrl: LoadingController,
        public viewCtrl: ViewController,
        public appCtrl: App,
        public modalCtrl: ModalController,
        public announcementService: AnnouncementService
    ) {

    }
  
    ionViewDidLoad() {
  
        console.log('ionViewDidLoad AnnouncementDetailPage');

        this.announcement = this.navParams.get('announcement');

        let user = JSON.parse(localStorage.getItem('user'));
        
        this.is_owner = user && user.id == this.announcement.user.id;
        this.is_admin = user && user.role.id == 1;

        this._announcementSubscription = this.announcementService.checkAnnouncement(this.announcement.id).subscribe((response: any) => {

            if (!response || (response && !response.status)) {

                this.alertCtrl.create({
                    title: 'Announcement',
                    subTitle: 'Announcement does not exists. It might be deleted by the admin or the owner. Refresh announcement list to get latest pending announcements.',
                    buttons: [
                        {
                            text: 'Okay',
                            handler: () => {
                                this.navCtrl.pop();
                            }
                        }
                    ]
                })
                .present();

                return;
            }

            this.announcementService.checkAnnouncementStatus(this.announcement.id).subscribe((response: any) => {

                if (!response || (response && !response.status)) {

                    this.alertCtrl.create({
                        title: 'Announcement',
                        subTitle: 'Announcement is either approved or declined already. Refresh announcement list to get latest pending announcements.',
                        buttons: [
                            {
                                text: 'Okay',
                                handler: () => {
                                    this.navCtrl.pop();
                                }
                            }
                        ]
                    })
                    .present();
    
                    return;
                }
            });
        });
    }

    ionViewWillLeave() {

        if (this._announcementSubscription) {
            this._announcementSubscription.unsubscribe();
        }
    }

    approve(): void {

        this.alertCtrl.create({
            title: 'Confirm Approve',
            subTitle: 'Are you sure you want to approve this announcement?',
            buttons: [
                {
                    text: 'Okay',
                    handler: () => {
                        
                        this._announcementSubscription = this.announcementService.approveAnnouncement(this.announcement.id).subscribe((response: any) => {

                            if (!response && (response && !response.status)) {

                                this.alertCtrl.create({
                                    title: 'Announcement',
                                    subTitle: 'Unable to approve annoucement. Please try again.',
                                    buttons: ['Okay']
                                });

                                return;
                            }

                            this.viewCtrl.dismiss({
                                approved: true,
                                id: this.announcement.id
                            });
                        });
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel'
                }
            ]
        })
        .present();
    }

    decline(): void {

        this.alertCtrl.create({
            title: 'Confirm Decline',
            subTitle: 'Are you sure you want to decline this announcement?',
            buttons: [
                {
                    text: 'Okay',
                    handler: () => {
                        
                        this._announcementSubscription = this.announcementService.declineAnnouncement(this.announcement.id).subscribe((response: any) => {

                            if (!response && (response && !response.status)) {

                                this.alertCtrl.create({
                                    title: 'Announcement',
                                    subTitle: 'Unable to decline annoucement. Please try again.',
                                    buttons: ['Okay']
                                });

                                return;
                            }

                            this.viewCtrl.dismiss({
                                declined: true,
                                id: this.announcement.id
                            });
                        });
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel'
                }
            ]
        })
        .present();
    }

}
