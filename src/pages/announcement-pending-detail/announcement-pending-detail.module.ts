import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AnnouncementPendingDetailPage } from './announcement-pending-detail';

@NgModule({
  declarations: [
    AnnouncementPendingDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(AnnouncementPendingDetailPage),
  ],
})
export class AnnouncementPendingDetailPageModule {}
