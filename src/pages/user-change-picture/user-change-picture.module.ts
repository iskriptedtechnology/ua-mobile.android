import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserChangePicturePage } from './user-change-picture';

@NgModule({
  declarations: [
    UserChangePicturePage,
  ],
  imports: [
    IonicPageModule.forChild(UserChangePicturePage),
  ],
})
export class UserChangePicturePageModule {}
