import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController, LoadingController } from 'ionic-angular';
import { Subscription } from 'rxjs/Subscription';
import { UserService } from '../user/user.service';

/**
 * Generated class for the UserChangePicturePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-user-change-picture',
    templateUrl: 'user-change-picture.html',
})
export class UserChangePicturePage {

    image: any;
    media_image: any;

    private _userSubscription: Subscription;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public viewCtrl: ViewController,
        public alertCtrl: AlertController,
        public loadingCtrl: LoadingController,
        private _userService: UserService
    ) {
    }
  
    ionViewDidLoad() {
        console.log('ionViewDidLoad UserChangePicturePage');
    }

    ionViewWillLeave() {

        if (this._userSubscription) {
            this._userSubscription.unsubscribe();
        }
    }

    selectImage(event: any): void {

        this.image = '';
        this.media_image = '';

        const files: any = event.dataTransfer ? event.dataTransfer.files : event.target.files;
        const pattern: RegExp = /image-*/;

        if (files && files.length > 0) {

            for (let i = 0; i <= files.length; i++) {

                const reader: FileReader = new FileReader();

                if (!files[i] || typeof files[i] === 'undefined') {
                    return;
                }

                if (!files[i].type.match(pattern)) {
                    return;
                }

                this.media_image = files[i];
                reader.onload = (e: any) => {
                    this.image = e.target.result;
                };
                reader.readAsDataURL(files[i]);
            }
        }
    }

    create(): void {

        if (!this.media_image) {

            this.viewCtrl.dismiss({
                announcement: {
                    posted: false
                }
            });

            return;
        }

        let loading = this.loadingCtrl.create({
            content: 'Updating Profile Picture...',
        });

        loading.present();

        this._userSubscription = this._userService.changePicture(this.media_image).subscribe((response: any) => {

            loading.dismiss();
            
            if (!response || (response && !response.status)) {

                this.alertCtrl.create({
                    title: 'User',
                    subTitle: 'Unable to change profile picture. Please try again',
                    buttons: ['Okay']
                })
                .present();

                return;
            }

            this.alertCtrl.create({
                title: 'Picture Changed',
                subTitle: 'Profile Picture changed successfully!',
                buttons: [{
                    text: 'Okay',
                    handler: () => {
                        this.viewCtrl.dismiss({
                            image: response.image
                        })
                    }
                }]
            })
            .present();

            this.image = '';
            this.media_image = '';
        });
    }

    close(): void {

        this.viewCtrl.dismiss();
    }

}
