import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, ViewController } from 'ionic-angular';
import { Subscription } from 'rxjs/Subscription';
import { AnnouncementService } from '../announcement/announcement.service';

/**
 * Generated class for the AnnouncementCreatePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-announcement-create',
    templateUrl: 'announcement-create.html',
})
export class AnnouncementCreatePage {

    name: string;
    description: string;
    image: string;
    media_image: string;
    date: string;
    time: string;
    course: any;
    section: any;

    courses: Array<any> = [];
    sections: Array<any> = ['A','B'];

    private _announcementSubscription: Subscription;

    constructor(
        public navCtrl: NavController, 
        public navParams: NavParams,
        public alertCtrl: AlertController,
        public loadingCtrl: LoadingController,
        public viewCtrl: ViewController,
        public announcementService: AnnouncementService
    ) {
    }

    ionViewDidLoad() {

        console.log('ionViewDidLoad AnnouncementCreatePage');

        this._announcementSubscription = this.announcementService.getCourses().subscribe((courses: any) => {

            if (!courses) {

                this.alertCtrl.create({
                    title: 'Courses',
                    subTitle: 'Unable to load courses!',
                    buttons: [{
                        text: 'Okay'
                    }]
                })
                .present();

                return;
            }

            this.courses = courses;
        });

        this.clearModelValues();
    }

    ionViewWillLeave() {

        if (this._announcementSubscription) {
            this._announcementSubscription.unsubscribe();
        }
    }
    
    selectImage(event: any): void {

        this.image = '';
        this.media_image = '';

        const files: any = event.dataTransfer ? event.dataTransfer.files : event.target.files;
        const pattern: RegExp = /image-*/;

        if (files && files.length > 0) {

            for (let i = 0; i <= files.length; i++) {

                const reader: FileReader = new FileReader();

                if (!files[i] || typeof files[i] === 'undefined') {
                    return;
                }

                if (!files[i].type.match(pattern)) {
                    return;
                }

                this.media_image = files[i];
                reader.onload = (e: any) => {
                    this.image = e.target.result;
                };
                reader.readAsDataURL(files[i]);
            }
        }
    }

    create(): void {

        if (!this.name) {
            this.alertCtrl.create({
                title: 'Field Required',
                subTitle: 'Name is required! Please do not leave it blank.',
                buttons: ['Okay']
            })
            .present();
            return;
        }
        if (!this.description) {
            this.alertCtrl.create({
                title: 'Field Required',
                subTitle: 'Description is required! Please do not leave it blank.',
                buttons: ['Okay']
            })
            .present();
            return;
        }
        if (!this.date) {
            this.alertCtrl.create({
                title: 'Field Required',
                subTitle: 'Date is required! Please do not leave it blank.',
                buttons: ['Okay']
            })
            .present();
            return;
        }
        if (!this.time) {
            this.alertCtrl.create({
                title: 'Field Required',
                subTitle: 'Time is required! Please do not leave it blank.',
                buttons: ['Okay']
            })
            .present();
            return;
        }

        let loading = this.loadingCtrl.create({
            content: 'Creating Announcement...',
        });

        loading.present();

        this._announcementSubscription = this.announcementService.createAnnouncement(
            this.name,
            this.description,
            this.date,
            this.time,
            this.media_image,
            this.course,
            this.section
        ).subscribe((response: any) => {

            loading.dismiss();

            if (!response || (response && !response.status)) {

                this.alertCtrl.create({
                    title: 'Announcement',
                    subTitle: 'Unable to create announcement. Please try again',
                    buttons: ['Okay']
                })
                .present();

                return;
            }

            this.alertCtrl.create({
                title: 'Announcement Created',
                subTitle: 'Announcement created successfully!',
                buttons: [{
                    text: 'Okay',
                    handler: () => {
                        this.viewCtrl.dismiss({
                            announcement: response.announcement
                        })
                    }
                }]
            })
            .present();

            this.clearModelValues();

        }, (Error: Error) => {

            loading.dismiss();

            this.alertCtrl.create({
                title: 'Announcement',
                subTitle: 'Unable to create announcement. Please try again',
                buttons: ['Okay']
            })
            .present();
        });
    }

    close(): void {

        this.viewCtrl.dismiss({
            announcement: {
                posted: false
            }
        });
    }

    clearModelValues(): void {

        this.name = '';
        this.description = '';
        this.date = '';
        this.time = '';
        this.media_image = '';
        this.image = '';
    }
}
