import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AnnouncementCreatePage } from './announcement-create';

@NgModule({
  declarations: [
    AnnouncementCreatePage,
  ],
  imports: [
    IonicPageModule.forChild(AnnouncementCreatePage),
  ],
})
export class AnnouncementCreatePageModule {}
