import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AnnouncementPendingPage } from './announcement-pending';

@NgModule({
  declarations: [
    AnnouncementPendingPage,
  ],
  imports: [
    IonicPageModule.forChild(AnnouncementPendingPage),
  ],
})
export class AnnouncementPendingPageModule {}
