import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewsPendingDetailPage } from './news-pending-detail';

@NgModule({
  declarations: [
    NewsPendingDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(NewsPendingDetailPage),
  ],
})
export class NewsPendingDetailPageModule {}
