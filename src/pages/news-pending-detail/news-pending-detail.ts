import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, ViewController, App, ModalController } from 'ionic-angular';
import { NewsService } from '../news/news.service';
import { Subscription } from 'rxjs/Subscription';

/**
 * Generated class for the NewsPendingDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-news-pending-detail',
    templateUrl: 'news-pending-detail.html',
})
export class NewsPendingDetailPage {

    news: any;
    is_owner: boolean;
    is_admin: boolean;

    private _newsSubscription: Subscription;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public alertCtrl: AlertController,
        public loadingCtrl: LoadingController,
        public viewCtrl: ViewController,
        public appCtrl: App,
        public modalCtrl: ModalController,
        public newsService: NewsService
    ) {

    }
  
    ionViewDidLoad() {
  
        console.log('ionViewDidLoad NewsDetailPage');

        this.news = this.navParams.get('news');

        let user = JSON.parse(localStorage.getItem('user'));
        
        this.is_owner = user && user.id == this.news.user.id;
        this.is_admin = user && user.role.id == 1;

        this._newsSubscription = this.newsService.checkNews(this.news.id).subscribe((response: any) => {

            if (!response || (response && !response.status)) {

                this.alertCtrl.create({
                    title: 'News',
                    subTitle: 'News does not exists. It might be deleted by the admin or the owner. Refresh news list to get latest pending news.',
                    buttons: [
                        {
                            text: 'Okay',
                            handler: () => {
                                this.navCtrl.pop();
                            }
                        }
                    ]
                })
                .present();

                return;
            }

            this.newsService.checkNewsStatus(this.news.id).subscribe((response: any) => {

                if (!response || (response && !response.status)) {

                    this.alertCtrl.create({
                        title: 'News',
                        subTitle: 'News is either approved or declined already. Refresh news list to get latest pending news.',
                        buttons: [
                            {
                                text: 'Okay',
                                handler: () => {
                                    this.navCtrl.pop();
                                }
                            }
                        ]
                    })
                    .present();
    
                    return;
                }
            });
        });
    }

    ionViewWillLeave() {

        if (this._newsSubscription) {
            this._newsSubscription.unsubscribe();
        }
    }

    approve(): void {

        this.alertCtrl.create({
            title: 'Confirm Approve',
            subTitle: 'Are you sure you want to approve this news?',
            buttons: [
                {
                    text: 'Okay',
                    handler: () => {
                        
                        this._newsSubscription = this.newsService.approveNews(this.news.id).subscribe((response: any) => {

                            if (!response && (response && !response.status)) {

                                this.alertCtrl.create({
                                    title: 'News',
                                    subTitle: 'Unable to approve news. Please try again.',
                                    buttons: ['Okay']
                                });

                                return;
                            }

                            this.viewCtrl.dismiss({
                                approved: true,
                                id: this.news.id
                            });
                        });
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel'
                }
            ]
        })
        .present();
    }

    decline(): void {

        this.alertCtrl.create({
            title: 'Confirm Decline',
            subTitle: 'Are you sure you want to decline this news?',
            buttons: [
                {
                    text: 'Okay',
                    handler: () => {
                        
                        this._newsSubscription = this.newsService.declineNews(this.news.id).subscribe((response: any) => {

                            if (!response && (response && !response.status)) {

                                this.alertCtrl.create({
                                    title: 'News',
                                    subTitle: 'Unable to decline news. Please try again.',
                                    buttons: ['Okay']
                                });

                                return;
                            }

                            this.viewCtrl.dismiss({
                                declined: true,
                                id: this.news.id
                            });
                        });
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel'
                }
            ]
        })
        .present();
    }

}
