import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, ViewController, App } from 'ionic-angular';
import { Subscription } from 'rxjs/Subscription';
import { EventService } from '../event/event.service';

/**
 * Generated class for the EventUpdatePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-event-update',
    templateUrl: 'event-update.html',
})
export class EventUpdatePage {

    event: any;

    id: number;
    name: string;
    description: string;
    image: string;
    media_image: string;
    date: string;
    time: string;
    course: any;
    section: any;

    courses: Array<any> = [];
    sections: Array<any> = ['A','B'];

    private _eventSubscription: Subscription;

    constructor(
        public navCtrl: NavController, 
        public navParams: NavParams,
        public alertCtrl: AlertController,
        public loadingCtrl: LoadingController,
        public eventService: EventService,
        public viewCtrl: ViewController,
        public appCtrl: App
    ) {
    }

    ionViewDidLoad() {

        console.log('ionViewDidLoad EventUpdatePage');

        this.clearModelValues();

        let data = this.navParams.get('event');

        this.event = data;
        this.id = data.id;
        this.name = data.name;
        this.description = data.description;
        this.image = data.image;
        this.date = data.raw_date;
        this.time = data.raw_time;
        this.course = data.course;
        this.section = data.section;

        this._eventSubscription = this.eventService.getCourses().subscribe((courses: any) => {

            if (!courses) {

                this.alertCtrl.create({
                    title: 'Courses',
                    subTitle: 'Unable to load courses!',
                    buttons: [{
                        text: 'Okay'
                    }]
                })
                .present();

                return;
            }

            this.courses = courses;
        });
    }

    ionViewWillLeave() {

        if (this._eventSubscription) {
            this._eventSubscription.unsubscribe();
        }
    }
    
    selectImage(event: any): void {

        this.image = '';
        this.media_image = '';

        const files: any = event.dataTransfer ? event.dataTransfer.files : event.target.files;
        const pattern: RegExp = /image-*/;

        if (files && files.length > 0) {

            for (let i = 0; i <= files.length; i++) {

                const reader: FileReader = new FileReader();

                if (!files[i] || typeof files[i] === 'undefined') {
                    return;
                }

                if (!files[i].type.match(pattern)) {
                    return;
                }

                this.media_image = files[i];
                reader.onload = (e: any) => {
                    this.image = e.target.result;
                };
                reader.readAsDataURL(files[i]);
            }
        }
    }

    update(): void {

        if (!this.name) {
            this.alertCtrl.create({
                title: 'Field Required',
                subTitle: 'Name is required! Please do not leave it blank.',
                buttons: ['Okay']
            })
            .present();
            return;
        }
        if (!this.description) {
            this.alertCtrl.create({
                title: 'Field Required',
                subTitle: 'Description is required! Please do not leave it blank.',
                buttons: ['Okay']
            })
            .present();
            return;
        }
        if (!this.date) {
            this.alertCtrl.create({
                title: 'Field Required',
                subTitle: 'Date is required! Please do not leave it blank.',
                buttons: ['Okay']
            })
            .present();
            return;
        }
        if (!this.time) {
            this.alertCtrl.create({
                title: 'Field Required',
                subTitle: 'Time is required! Please do not leave it blank.',
                buttons: ['Okay']
            })
            .present();
            return;
        }

        let loading = this.loadingCtrl.create({
            content: 'Updating Event...',
        });

        loading.present();

        this._eventSubscription = this.eventService.updateEvent(
            this.id,
            this.name,
            this.description,
            this.date,
            this.time,
            this.media_image,
            this.course,
            this.section
        ).subscribe((response: any) => {

            loading.dismiss();

            if (!response || (response && !response.status)) {

                this.alertCtrl.create({
                    title: 'Event',
                    subTitle: 'Unable to update event. Please try again',
                    buttons: ['Okay']
                })
                .present();

                return;
            }

            this.alertCtrl.create({
                title: 'Event Updated',
                subTitle: 'Event has been updated successfully!',
                buttons: [{
                    text: 'Okay',
                    handler: () => {
                        this.viewCtrl.dismiss({
                            event: response.event
                        });
                    }
                }]
            })
            .present();

        }, (Error: Error) => {

            loading.dismiss();

            this.alertCtrl.create({
                title: 'Event',
                subTitle: 'Unable to update event. Please try again',
                buttons: ['Okay']
            })
            .present();
        });
    }

    close(): void {

        this.viewCtrl.dismiss();
    }

    clearModelValues(): void {

        this.id = null;
        this.name = '';
        this.description = '';
        this.date = '';
        this.time = '';
        this.media_image = '';
        this.image = '';
    }
}
