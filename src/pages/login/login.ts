import { Component } from '@angular/core';
import { IonicPage, AlertController, NavController, LoadingController } from 'ionic-angular';
import { LoginService } from './login.service';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-login',
    templateUrl: 'login.html',
})
export class LoginPage {

    username: string;
    password: string;
    
    constructor(
        public navCtrl: NavController,
        public alertCtrl: AlertController,
        private loginService: LoginService,
        public loadingCtrl: LoadingController
    ) {
    }
  
    ionViewDidLoad() {
  
        console.log('ionViewDidLoad LoginPage');
    }

    authenticate(): void {

        if (!this.username) {
            this.alertCtrl.create({
                subTitle: 'Username is required!',
                buttons: ['OK']
            })
            .present();
            return;
        }
        if(!this.password) {
            this.alertCtrl.create({
                subTitle: 'Password is required!',
                buttons: ['Okay']
            })
            .present();
            return;
        }

        let loading = this.loadingCtrl.create({
            content: 'Logging in...',
        });

        loading.present();

        this.loginService.authenticate(this.username, this.password).subscribe((response: any) => {

            loading.dismiss();

            if (response && response.error === 'unauthorized') {

                this.alertCtrl.create({
                    subTitle: 'Invalid credentials. Please try again!',
                    buttons: ['Okay']
                })
                .present();

                return;
            }

            localStorage.setItem('tkn', response.access_token);
            localStorage.setItem('user', JSON.stringify(response.user));

            this.navCtrl.setRoot('HomePage');

        }, (error: Error) => {

            console.log(error);

            loading.dismiss();

            this.alertCtrl.create({
                subTitle: error.message,
                buttons: ['Okay']
            })
            .present();
        });
    }

}
