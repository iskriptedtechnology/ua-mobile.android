import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import { AppService } from '../../app/app.service';

@Injectable()
export class LoginService {

    constructor(
        public httpClient: HttpClient,
        public appService: AppService
    ) {

    }

    public authenticate(username: string, password: string): Observable<Response> {

        const data: URLSearchParams = new URLSearchParams();

        data.append('username', username);
        data.append('password', password);

        return this.httpClient.post(this.appService.API_ENDPOINT + 'authenticate', data.toString()).do((response: any) => response);
    }

    public logout(): Observable<Response> {

        const data: URLSearchParams = new URLSearchParams();

        return this.httpClient.post(this.appService.API_ENDPOINT + 'logout', data.toString()).do((response: any) => response);
    }
}