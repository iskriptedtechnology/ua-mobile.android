import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, ViewController, App, ModalController } from 'ionic-angular';
import { AnnouncementService } from '../announcement/announcement.service';
import { Subscription } from 'rxjs/Subscription';

/**
 * Generated class for the AnnouncementDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-announcement-detail',
    templateUrl: 'announcement-detail.html',
})
export class AnnouncementDetailPage {

    announcement: any;
    is_owner: boolean;
    is_admin: boolean;

    private _announcementSubscription: Subscription;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public alertCtrl: AlertController,
        public loadingCtrl: LoadingController,
        public viewCtrl: ViewController,
        public appCtrl: App,
        public modalCtrl: ModalController,
        public announcementService: AnnouncementService
    ) {

    }
  
    ionViewDidLoad() {
  
        console.log('ionViewDidLoad AnnouncementDetailPage');

        this.announcement = this.navParams.get('announcement');

        let user = JSON.parse(localStorage.getItem('user'));
        
        this.is_owner = user && user.id == this.announcement.user.id;
        this.is_admin = user && user.role.id == 1;

        this._announcementSubscription = this.announcementService.checkAnnouncement(this.announcement.id).subscribe((response: any) => {

            if (!response || (response && !response.status)) {

                this.alertCtrl.create({
                    title: 'Announcement',
                    subTitle: 'Announcement does not exists. It might be deleted by the admin or the owner. Refresh announcement list to get latest announcements.',
                    buttons: [
                        {
                            text: 'Okay',
                            handler: () => {
                                this.navCtrl.pop();
                            }
                        }
                    ]
                })
                .present();
            }
        });
    }

    ionViewWillLeave() {

        if (this._announcementSubscription) {
            this._announcementSubscription.unsubscribe();
        }
    }

    update(): void {

        let profileModal = this.modalCtrl.create('AnnouncementUpdatePage', { 
            announcement: this.announcement
        });
        profileModal.onDidDismiss((data: any) => {

            if (data && data.announcement) {

                this.announcement = data.announcement;
            }
        });
        profileModal.present();
    }

    delete(): void {

        this.alertCtrl.create({
            title: 'Confirm Delete',
            subTitle: 'Are you sure you want to delete this announcement?',
            buttons: [
                {
                    text: 'Okay',
                    handler: () => {
                        
                        this._announcementSubscription = this.announcementService.deleteAnnouncement(this.announcement.id).subscribe((response: any) => {

                            if (!response && (response && !response.status)) {

                                this.alertCtrl.create({
                                    title: 'Announcement',
                                    subTitle: 'Unable to delete annoucement. Please try again.',
                                    buttons: ['Okay']
                                });

                                return;
                            }

                            this.viewCtrl.dismiss({
                                deleted: true,
                                id: this.announcement.id
                            });
                        });
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel'
                }
            ]
        })
        .present();
    }
}
