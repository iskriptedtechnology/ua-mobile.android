import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, ViewController, App, ModalController } from 'ionic-angular';
import { EventService } from '../event/event.service';
import { Subscription } from 'rxjs/Subscription';

/**
 * Generated class for the EventDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-event-detail',
    templateUrl: 'event-detail.html',
})
export class EventDetailPage {

    event: any;
    is_owner: boolean;
    is_admin: boolean;

    private _eventSubscription: Subscription;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public alertCtrl: AlertController,
        public loadingCtrl: LoadingController,
        public viewCtrl: ViewController,
        public appCtrl: App,
        public modalCtrl: ModalController,
        public eventService: EventService
    ) {

    }
  
    ionViewDidLoad() {
  
        console.log('ionViewDidLoad EventDetailPage');

        this.event = this.navParams.get('event');

        let user = JSON.parse(localStorage.getItem('user'));
        
        this.is_owner = user && user.id == this.event.user.id;
        this.is_admin = user && user.role.id == 1;

        this._eventSubscription = this.eventService.checkEvent(this.event.id).subscribe((response: any) => {

            if (!response || (response && !response.status)) {

                this.alertCtrl.create({
                    title: 'Event',
                    subTitle: 'Event does not exists. It might be deleted by the admin or the owner. Refresh events list to get latest events.',
                    buttons: [
                        {
                            text: 'Okay',
                            handler: () => {
                                this.navCtrl.pop();
                            }
                        }
                    ]
                })
                .present();
            }
        });
    }

    ionViewWillLeave() {

        if (this._eventSubscription) {
            this._eventSubscription.unsubscribe();
        }
    }

    update(): void {

        let profileModal = this.modalCtrl.create('EventUpdatePage', { 
            event: this.event
        });
        profileModal.onDidDismiss((data: any) => {

            if (data && data.event) {

                this.event = data.event;
            }
        });
        profileModal.present();
    }

    delete(): void {

        this.alertCtrl.create({
            title: 'Confirm Delete',
            subTitle: 'Are you sure you want to delete this event?',
            buttons: [
                {
                    text: 'Okay',
                    handler: () => {
                        
                        this._eventSubscription = this.eventService.deleteEvent(this.event.id).subscribe((response: any) => {

                            if (!response && (response && !response.status)) {

                                this.alertCtrl.create({
                                    title: 'Event',
                                    subTitle: 'Unable to delete event. Please try again.',
                                    buttons: ['Okay']
                                });

                                return;
                            }

                            this.viewCtrl.dismiss({
                                deleted: true,
                                id: this.event.id
                            });
                        });
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel'
                }
            ]
        })
        .present();
    }

}
