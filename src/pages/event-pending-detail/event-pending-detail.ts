import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, ViewController, App, ModalController } from 'ionic-angular';
import { EventService } from '../event/event.service';
import { Subscription } from 'rxjs/Subscription';

/**
 * Generated class for the EventPendingDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-event-pending-detail',
    templateUrl: 'event-pending-detail.html',
})
export class EventPendingDetailPage {

    event: any;
    is_owner: boolean;
    is_admin: boolean;

    private _eventSubscription: Subscription;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public alertCtrl: AlertController,
        public loadingCtrl: LoadingController,
        public viewCtrl: ViewController,
        public appCtrl: App,
        public modalCtrl: ModalController,
        public eventService: EventService
    ) {

    }
  
    ionViewDidLoad() {
  
        console.log('ionViewDidLoad EventDetailPage');

        this.event = this.navParams.get('event');

        let user = JSON.parse(localStorage.getItem('user'));
        
        this.is_owner = user && user.id == this.event.user.id;
        this.is_admin = user && user.role.id == 1;

        this._eventSubscription = this.eventService.checkEvent(this.event.id).subscribe((response: any) => {

            if (!response || (response && !response.status)) {

                this.alertCtrl.create({
                    title: 'Event',
                    subTitle: 'Event does not exists. It might be deleted by the admin or the owner. Refresh event list to get latest pending events.',
                    buttons: [
                        {
                            text: 'Okay',
                            handler: () => {
                                this.navCtrl.pop();
                            }
                        }
                    ]
                })
                .present();

                return;
            }

            this.eventService.checkEventStatus(this.event.id).subscribe((response: any) => {

                if (!response || (response && !response.status)) {

                    this.alertCtrl.create({
                        title: 'Event',
                        subTitle: 'Event is either approved or declined already. Refresh event list to get latest pending events.',
                        buttons: [
                            {
                                text: 'Okay',
                                handler: () => {
                                    this.navCtrl.pop();
                                }
                            }
                        ]
                    })
                    .present();
    
                    return;
                }
            });
        });
    }

    ionViewWillLeave() {

        if (this._eventSubscription) {
            this._eventSubscription.unsubscribe();
        }
    }

    approve(): void {

        this.alertCtrl.create({
            title: 'Confirm Approve',
            subTitle: 'Are you sure you want to approve this announcement?',
            buttons: [
                {
                    text: 'Okay',
                    handler: () => {
                        
                        this._eventSubscription = this.eventService.approveEvent(this.event.id).subscribe((response: any) => {

                            if (!response && (response && !response.status)) {

                                this.alertCtrl.create({
                                    title: 'Event',
                                    subTitle: 'Unable to approve event. Please try again.',
                                    buttons: ['Okay']
                                });

                                return;
                            }

                            this.viewCtrl.dismiss({
                                approved: true,
                                id: this.event.id
                            });
                        });
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel'
                }
            ]
        })
        .present();
    }

    decline(): void {

        this.alertCtrl.create({
            title: 'Confirm Decline',
            subTitle: 'Are you sure you want to decline this event?',
            buttons: [
                {
                    text: 'Okay',
                    handler: () => {
                        
                        this._eventSubscription = this.eventService.declineEvent(this.event.id).subscribe((response: any) => {

                            if (!response && (response && !response.status)) {

                                this.alertCtrl.create({
                                    title: 'Event',
                                    subTitle: 'Unable to decline annoucement. Please try again.',
                                    buttons: ['Okay']
                                });

                                return;
                            }

                            this.viewCtrl.dismiss({
                                declined: true,
                                id: this.event.id
                            });
                        });
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel'
                }
            ]
        })
        .present();
    }

}
