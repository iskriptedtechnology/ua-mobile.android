import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EventPendingDetailPage } from './event-pending-detail';

@NgModule({
  declarations: [
    EventPendingDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(EventPendingDetailPage),
  ],
})
export class EventPendingDetailPageModule {}
