import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewsCreatePage } from './news-create';

@NgModule({
  declarations: [
    NewsCreatePage,
  ],
  imports: [
    IonicPageModule.forChild(NewsCreatePage),
  ],
})
export class NewsCreatePageModule {}
