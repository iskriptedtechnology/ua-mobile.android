import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EventPendingPage } from './event-pending';

@NgModule({
  declarations: [
    EventPendingPage,
  ],
  imports: [
    IonicPageModule.forChild(EventPendingPage),
  ],
})
export class EventPendingPageModule {}
