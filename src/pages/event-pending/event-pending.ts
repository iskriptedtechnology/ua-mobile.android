import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { Subscription } from 'rxjs/Subscription';
import { EventService } from '../event/event.service';

/**
 * Generated class for the EventPendingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-event-pending',
    templateUrl: 'event-pending.html',
})
export class EventPendingPage {

    events: Array<any> = [];
    is_loading: boolean;
    is_error: boolean;
    is_admin: boolean;

    private _eventSubscription: Subscription;
  
    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public modalCtrl: ModalController,
        public eventService: EventService
    ) {
  
    }
  
    ionViewDidLoad() {
  
        console.log('ionViewDidLoad AnnouncementPendingPage');
  
        this.is_loading = true;
        this.is_error = false;
        
        let user = JSON.parse(localStorage.getItem('user'));
  
        this.is_admin = user && user.role.id != 4;
  
        this._eventSubscription = this.eventService.getAllPending().subscribe((response: any) => {
  
            this.is_loading = false;
  
            if (!response) {
  
                this.is_error = true;
                this.events = [];
            }
  
            this.events = response.events;
  
        }, (error: Error) => {
  
            this.is_loading = false;
            this.is_error = true;
        });
    }
  
    ionViewWillLeave() {
  
        if (this._eventSubscription) {
            this._eventSubscription.unsubscribe();
        }
    }
  
    doRefresh(refresher): void {
  
        console.log('Begin async operation', refresher);
  
        this.is_loading = false;
        this.is_error = false;
  
        this._eventSubscription = this.eventService.getAllPending().subscribe((response: any) => {
  
            if (!response) {
  
                this.is_error = true;
                this.events = [];
            }
  
            this.events = response.events;
  
            refresher.complete();
  
        }, (error: Error) => {
  
            this.is_error = true;
  
            refresher.complete();
        });
    }
  
    goToDetails(event: any): void {
  
        this.navCtrl.push('EventPendingDetailPage', {
            event: event
        });
    }

}
