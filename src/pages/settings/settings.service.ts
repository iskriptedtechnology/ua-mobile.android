import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppService } from '../../app/app.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class SettingService {

    constructor(
        public httpClient: HttpClient,
        public appService: AppService
    ) {

    }

    public getPendingPostCounts(): Observable<Response> {

        return this.httpClient.get(this.appService.API_ENDPOINT + 'post/pending/count').do((response: any) => response);
    }
}