import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController, App, PopoverController } from 'ionic-angular';
import { LoginService } from '../login/login.service';
import { SettingService } from './settings.service';
import { Subscription } from 'rxjs/Subscription';

/**
 * Generated class for the SettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-settings',
    templateUrl: 'settings.html',
})
export class SettingsPage {

    count: any;
    is_admin: boolean;

    private _settingsSubscription: Subscription;

    constructor(
        public navCtrl: NavController, 
        public navParams: NavParams,
        public loadingCtrl: LoadingController,
        public loginService: LoginService,
        public alertCtrl: AlertController,
        public popoverCtrl: PopoverController,
        public app: App,
        public settingService: SettingService
    ) {
    }

    ionViewWillEnter() {

        let user = JSON.parse(localStorage.getItem('user'));

        if (user.role.id != 3) {

            this._settingsSubscription = this.settingService.getPendingPostCounts().subscribe((response: any) => {

                if (!response) {
    
                    this.alertCtrl.create({
                        title: 'Error',
                        subTitle: 'Cannot fetch count of pending posts.',
                        buttons: ['Okay']
                    })
                    .present();
    
                    return;
                }
    
                this.count = response;
            });
        }
    }

    ionViewDidLoad() {

        console.log('ionViewDidLoad SettingsPage');

        let user = JSON.parse(localStorage.getItem('user'));

        this.is_admin = user && (user.role.id == 1 || user.role.id == 2);
    }

    ionViewWillLeave() {

        if (this._settingsSubscription) {
            this._settingsSubscription.unsubscribe();
        }
    }

    goToPendingAnnouncements(): void {

        this.navCtrl.push('AnnouncementPendingPage');
    }

    goToPendingNews(): void {

        this.navCtrl.push('NewsPendingPage');
    }

    goToPendingEvents(): void {

        this.navCtrl.push('EventPendingPage');
    }

    goToAbout(): void {

        this.navCtrl.push('AboutPage');
    }

    goToHelp(): void {

        this.navCtrl.push('HelpPage');
    }

    goToFaqs(): void {

        this.navCtrl.push('FaqsPage');
    }

    logout(): void {

        let loading = this.loadingCtrl.create({
            content: 'Logging out...',
        });

        loading.present();

        this.loginService.logout().subscribe((response: any) => {

            loading.dismiss();

            if (!response) {

                this.alertCtrl.create({
                    subTitle: 'Unable to logout. Please try again!',
                    buttons: ['Okay']
                })
                .present();
            }

            localStorage.clear();

            this.app.getRootNav().setRoot('LoginPage');

        }, (error: Error) => {

            loading.dismiss();
        });
    }

}
