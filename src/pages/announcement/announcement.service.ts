import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import { AppService } from '../../app/app.service';

@Injectable()
export class AnnouncementService {

    prefix: string = 'announcement/';

    constructor(
        public httpClient: HttpClient,
        public appService: AppService
    ) {

    }

    public getAll(): Observable<Response> {

        return this.httpClient.get(this.appService.API_ENDPOINT + this.prefix + 'all/get').do((response: any) => response);
    }

    public getAllPending(): Observable<Response> {

        return this.httpClient.get(this.appService.API_ENDPOINT + this.prefix +  'pending/get').do((response: any) => response);
    }

    public getDetails(id: number): Observable<Response> {

        return this.httpClient.get(this.appService.API_ENDPOINT + this.prefix + 'detail/get/' + id).do((response: any) => response);
    }

    public createAnnouncement(name: string, description: string, date: string, time: string, image: string, course: any, section: any): Observable<Response> {

        const data: FormData = new FormData();

        data.append('name', name);
        data.append('description', description);
        data.append('date', date);
        data.append('time', time);
        data.append('course_id', course == null ? 0 : course);
        data.append('section', section);

        if (image != null) {
            data.append('image', image, image['name']);
        }

        return this.httpClient.post(this.appService.API_ENDPOINT + this.prefix + 'upload/create', data).do((response: any) => response);
    }

    public updateAnnouncement(id: number, name: string, description: string, date: string, time: string, image: string,  course: any, section: any): Observable<Response> {

        const data: FormData = new FormData();

        data.append('id', id.toString());
        data.append('name', name);
        data.append('description', description);
        data.append('date', date);
        data.append('time', time);
        data.append('course_id', course == null ? 0 : course);
        data.append('section', section);

        if (image != null) {
            data.append('image', image, image['name']);
        }

        return this.httpClient.post(this.appService.API_ENDPOINT + this.prefix + 'upload/update', data).do((response: any) => response);
    }

    public deleteAnnouncement(id: number): Observable<Response> {

        return this.httpClient.delete(this.appService.API_ENDPOINT + this.prefix + 'delete/' + id).do((response: any) => response);
    }

    public checkAnnouncement(id: number): Observable<Response> {

        return this.httpClient.get(this.appService.API_ENDPOINT + this.prefix + 'check/' + id).do((response: any) => response);
    }

    public checkAnnouncementStatus(id: number): Observable<Response> {

        return this.httpClient.get(this.appService.API_ENDPOINT + this.prefix + 'status/check/' + id).do((response: any) => response);
    }

    public approveAnnouncement(id: number): Observable<Response> {

        return this.httpClient.get(this.appService.API_ENDPOINT + this.prefix + 'approve/' + id).do((response: any) => response);
    }

    public declineAnnouncement(id: number): Observable<Response> {

        return this.httpClient.get(this.appService.API_ENDPOINT + this.prefix + 'decline/' + id).do((response: any) => response);
    }

    public getCourses(): Observable<Response> {

        return this.httpClient.get(this.appService.API_ENDPOINT + 'course/all/get').do((response: any) => response);
    }
}