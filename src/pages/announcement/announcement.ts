import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { Subscription } from 'rxjs/Subscription';
import { AnnouncementService } from './announcement.service';

/**
 * Generated class for the AnnouncementPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-announcement',
    templateUrl: 'announcement.html',
})
export class AnnouncementPage {

    announcements: Array<any> = [];
    is_loading: boolean;
    is_error: boolean;
    is_admin: boolean;

    private _announcementSubscription: Subscription;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public modalCtrl: ModalController,
        public announcementService: AnnouncementService
    ) {

    }
  
    ionViewDidLoad() {

        console.log('ionViewDidLoad AnnouncementPage');

        this.is_loading = true;
        this.is_error = false;
        
        let user = JSON.parse(localStorage.getItem('user'));

        this.is_admin = user && user.role.id != 4;

        this._announcementSubscription = this.announcementService.getAll().subscribe((response: any) => {

            this.is_loading = false;

            if (!response) {

                this.is_error = true;
                this.announcements = [];
            }

            this.announcements = response.announcements;

        }, (error: Error) => {

            this.is_loading = false;
            this.is_error = true;
        });
    }

    ionViewWillLeave() {

        if (this._announcementSubscription) {
            this._announcementSubscription.unsubscribe();
        }
    }

    doRefresh(refresher): void {

        console.log('Begin async operation', refresher);

        this.is_loading = false;
        this.is_error = false;

        this._announcementSubscription = this.announcementService.getAll().subscribe((response: any) => {

            if (!response) {

                this.is_error = true;
                this.announcements = [];
            }

            this.announcements = response.announcements;

            refresher.complete();

        }, (error: Error) => {

            this.is_error = true;

            refresher.complete();
        });
    }

    goToDetails(announcement: any): void {

        this.navCtrl.push('AnnouncementDetailPage', {
            announcement: announcement
        });
    }

    create(): void {

        let profileModal = this.modalCtrl.create('AnnouncementCreatePage');
        profileModal.onDidDismiss((data: any) => {
            
            if (data && (data.announcement && data.announcement.posted)) {

                this.announcements.unshift(data.announcement);
            }
        });
        profileModal.present();
    }
}
