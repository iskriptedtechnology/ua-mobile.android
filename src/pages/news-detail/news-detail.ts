import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, ViewController, App, ModalController } from 'ionic-angular';
import { Subscription } from 'rxjs/Subscription';
import { NewsService } from '../news/news.service';

/**
 * Generated class for the NewsDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-news-detail',
    templateUrl: 'news-detail.html',
})
export class NewsDetailPage {

    news: any;
    is_owner: boolean;
    is_admin: boolean;

    private _newsSubscription: Subscription;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public alertCtrl: AlertController,
        public loadingCtrl: LoadingController,
        public viewCtrl: ViewController,
        public appCtrl: App,
        public modalCtrl: ModalController,
        public newsService: NewsService
    ) {

    }
  
    ionViewDidLoad() {
  
        console.log('ionViewDidLoad NewsDetailPage');

        this.news = this.navParams.get('news');

        let user = JSON.parse(localStorage.getItem('user'));
        
        this.is_owner = user && user.id == this.news.user.id;
        this.is_admin = user && user.role.id == 1;

        this._newsSubscription = this.newsService.checkNews(this.news.id).subscribe((response: any) => {

            if (!response || (response && !response.status)) {

                this.alertCtrl.create({
                    title: 'News',
                    subTitle: 'News does not exists. It might be deleted by the admin or the owner. Refresh news list to get latest news.',
                    buttons: [
                        {
                            text: 'Okay',
                            handler: () => {
                                this.navCtrl.pop();
                            }
                        }
                    ]
                })
                .present();
            }
        });
    }

    ionViewWillLeave() {

        if (this._newsSubscription) {
            this._newsSubscription.unsubscribe();
        }
    }

    update(): void {

        let profileModal = this.modalCtrl.create('NewsUpdatePage', { 
            news: this.news
        });
        profileModal.onDidDismiss((data: any) => {

            if (data && data.news) {

                this.news = data.news;
            }
        });
        profileModal.present();
    }

    delete(): void {

        this.alertCtrl.create({
            title: 'Confirm Delete',
            subTitle: 'Are you sure you want to delete this news?',
            buttons: [
                {
                    text: 'Okay',
                    handler: () => {
                        
                        this._newsSubscription = this.newsService.deleteNews(this.news.id).subscribe((response: any) => {

                            if (!response && (response && !response.status)) {

                                this.alertCtrl.create({
                                    title: 'News',
                                    subTitle: 'Unable to delete news. Please try again.',
                                    buttons: ['Okay']
                                });

                                return;
                            }

                            this.viewCtrl.dismiss({
                                deleted: true,
                                id: this.news.id
                            });
                        });
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel'
                }
            ]
        })
        .present();
    }
}
