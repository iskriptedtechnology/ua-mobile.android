import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewsPendingPage } from './news-pending';

@NgModule({
  declarations: [
    NewsPendingPage,
  ],
  imports: [
    IonicPageModule.forChild(NewsPendingPage),
  ],
})
export class NewsPendingPageModule {}
