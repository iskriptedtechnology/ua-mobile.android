import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/do';
import { AppService } from '../../app/app.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class UserService {

    prefix: string = 'user/';

    constructor(
        public httpClient: HttpClient,
        public appService: AppService
    ) {

    }

    public getDetails(): Observable<Response> {

        return this.httpClient.get(this.appService.API_ENDPOINT + 'me').do((response: any) => response);
    }

    public changePicture(image: string): Observable<Response> {

        const data: FormData = new FormData();

        data.append('image', image, image['name']);

        return this.httpClient.post(this.appService.API_ENDPOINT + this.prefix + 'upload/change-picture', data).do((response: any) => response);
    }

    public changePassword(old_password: string, password: string, password_confirmation: string): Observable<Response> {

        const data: URLSearchParams = new URLSearchParams();

        data.append('old_password', old_password);
        data.append('password', password);
        data.append('password_confirmation', password_confirmation);

        return this.httpClient.post(this.appService.API_ENDPOINT + this.prefix + 'change-password', data.toString()).do((response: any) => response);
    }
}
