import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ModalController } from 'ionic-angular';
import { Subscription } from 'rxjs/Subscription';
import { UserService } from './user.service';

/**
 * Generated class for the UserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-user',
    templateUrl: 'user.html',
})
export class UserPage {

    user: any;

    private _userSubscription: Subscription;

    constructor(
        public navCtrl: NavController, 
        public navParams: NavParams,
        public alertCtrl: AlertController,
        public modalCtrl: ModalController,
        private _userService: UserService
    ) {
    }
  
    ionViewDidLoad() {
        console.log('ionViewDidLoad UserPage');

        this._userSubscription = this._userService.getDetails().subscribe((user: any) => {

            if (!user) {

                this.alertCtrl.create({
                    title: 'Profile',
                    subTitle: 'Unable to get user details. Please try again.',
                    buttons: [
                        {
                            text: 'Okay',
                            handler: () => {
                                this.navCtrl.pop();
                            }
                        }
                    ]
                })
                .present();
            }

            this.user = user;
        });
    }

    ionViewWillLeave() {

        if (this._userSubscription) {
            this._userSubscription.unsubscribe();
        }
    }

    changePicture(): void {

        let changePictureModal = this.modalCtrl.create('UserChangePicturePage');
        changePictureModal.onDidDismiss((data: any) => {
            
            if (data && data.image) {
                this.user.details.image = data.image;
            }
        });
        changePictureModal.present();
    }

    changePassword(): void {

        let changePasswordModal = this.modalCtrl.create('UserChangePasswordPage');
        
        changePasswordModal.present();
    }

}
