import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import { AppService } from '../../app/app.service';

@Injectable()
export class NewsService {

    prefix: string = 'news/';

    constructor(
        public httpClient: HttpClient,
        public appService: AppService
    ) {

    }

    public getAll(): Observable<Response> {

        return this.httpClient.get(this.appService.API_ENDPOINT + this.prefix + 'all/get').do((response: any) => response);
    }

    public getAllPending(): Observable<Response> {

        return this.httpClient.get(this.appService.API_ENDPOINT + this.prefix +  'pending/get').do((response: any) => response);
    }

    public getDetails(id: number): Observable<Response> {

        return this.httpClient.get(this.appService.API_ENDPOINT + this.prefix + 'detail/get/' + id).do((response: any) => response);
    }

    public createNews(name: string, description: string, date: string, time: string, image: string, course: any, section: any): Observable<Response> {

        const data: FormData = new FormData();

        data.append('name', name);
        data.append('description', description);
        data.append('duration_until', date);
        data.append('time', time);
        data.append('course_id', course == null ? 0 : course);
        data.append('section', section);

        if (image != null) {
            data.append('image', image, image['name']);
        }

        return this.httpClient.post(this.appService.API_ENDPOINT + this.prefix + 'upload/create', data).do((response: any) => response);
    }

    public updateNews(id: number, name: string, description: string, date: string, time: string, image: string, course: any, section: any): Observable<Response> {

        const data: FormData = new FormData();

        data.append('id', id.toString());
        data.append('name', name);
        data.append('description', description);
        data.append('duration_until', date);
        data.append('time', time);
        data.append('course_id', course == null ? 0 : course);
        data.append('section', section);

        if (image != null) {
            data.append('image', image, image['name']);
        }

        return this.httpClient.post(this.appService.API_ENDPOINT + this.prefix + 'upload/update', data).do((response: any) => response);
    }

    public deleteNews(id: number): Observable<Response> {

        return this.httpClient.delete(this.appService.API_ENDPOINT + this.prefix + 'delete/' + id).do((response: any) => response);
    }

    public checkNews(id: number): Observable<Response> {

        return this.httpClient.get(this.appService.API_ENDPOINT + this.prefix + 'check/' + id).do((response: any) => response);
    }

    public checkNewsStatus(id: number): Observable<Response> {

        return this.httpClient.get(this.appService.API_ENDPOINT + this.prefix + 'status/check/' + id).do((response: any) => response);
    }

    public approveNews(id: number): Observable<Response> {

        return this.httpClient.get(this.appService.API_ENDPOINT + this.prefix + 'approve/' + id).do((response: any) => response);
    }

    public declineNews(id: number): Observable<Response> {

        return this.httpClient.get(this.appService.API_ENDPOINT + this.prefix + 'decline/' + id).do((response: any) => response);
    }

    public getCourses(): Observable<Response> {

        return this.httpClient.get(this.appService.API_ENDPOINT + 'course/all/get').do((response: any) => response);
    }
}