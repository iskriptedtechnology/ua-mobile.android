import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { Subscription } from 'rxjs/Subscription';
import { NewsService } from './news.service';

/**
 * Generated class for the NewsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-news',
    templateUrl: 'news.html',
})
export class NewsPage {

    news: Array<any> = [];
    is_loading: boolean;
    is_error: boolean;
    is_admin: boolean;

    private _newsSubscription: Subscription;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public modalCtrl: ModalController,
        public newsService: NewsService
    ) {

    }
  
    ionViewDidLoad() {

        console.log('ionViewDidLoad NewsPage');

        this.is_loading = true;
        this.is_error = false;

        let user = JSON.parse(localStorage.getItem('user'));

        this.is_admin = user && user.role.id != 4;

        this._newsSubscription = this.newsService.getAll().subscribe((response: any) => {

            this.is_loading = false;

            if (!response) {

                this.is_error = true;
                this.news = [];
            }

            this.news = response.news;

        }, (error: Error) => {

            this.is_loading = false;
            this.is_error = true;
        });
    }

    ionViewWillLeave() {

        if (this._newsSubscription) {
            this._newsSubscription.unsubscribe();
        }
    }

    doRefresh(refresher): void {

        console.log('Begin async operation', refresher);

        this.is_loading = false;
        this.is_error = false;

        this._newsSubscription = this.newsService.getAll().subscribe((response: any) => {

            if (!response) {

                this.is_error = true;
                this.news = [];
            }

            this.news = response.news;

            refresher.complete();

        }, (error: Error) => {

            this.is_error = true;

            refresher.complete();
        });
    }

    goToDetails(news: any): void {

        this.navCtrl.push('NewsDetailPage', {
            news: news
        });
    }

    create(): void {

        let profileModal = this.modalCtrl.create('NewsCreatePage');
        profileModal.onDidDismiss((data: any) => {
            
            if (data && (data.news && data.news.posted)) {

                this.news.unshift(data.news);
            }
        });
        profileModal.present();
    }
}
