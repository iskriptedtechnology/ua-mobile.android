import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AnnouncementUpdatePage } from './announcement-update';

@NgModule({
    declarations: [
        AnnouncementUpdatePage,
    ],
    imports: [
        IonicPageModule.forChild(AnnouncementUpdatePage)
    ],
})
export class AnnouncementUpdatePageModule {}
