import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { Subscription } from 'rxjs/Subscription';
import { EventService } from './event.service';

/**
 * Generated class for the EventPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-event',
    templateUrl: 'event.html',
})
export class EventPage {

    events: Array<any> = [];
    is_loading: boolean;
    is_error: boolean;
    is_admin: boolean;

    private _eventSubscription: Subscription;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public modalCtrl: ModalController,
        public eventService: EventService
    ) {

    }
  
    ionViewDidLoad() {

        console.log('ionViewDidLoad EventPage');

        this.is_loading = true;
        this.is_error = false;

        let user = JSON.parse(localStorage.getItem('user'));

        this.is_admin = user && user.role.id != 4;

        this._eventSubscription = this.eventService.getAll().subscribe((response: any) => {

            this.is_loading = false;

            if (!response) {

                this.is_error = true;
                this.events = [];
            }

            this.events = response.events;

        }, (error: Error) => {

            this.is_loading = false;
            this.is_error = true;
        });
    }

    ionViewWillLeave() {

        if (this._eventSubscription) {
            this._eventSubscription.unsubscribe();
        }
    }

    doRefresh(refresher): void {

        console.log('Begin async operation', refresher);

        this.is_loading = false;
        this.is_error = false;

        this._eventSubscription = this.eventService.getAll().subscribe((response: any) => {

            if (!response) {

                this.is_error = true;
                this.events = [];
            }

            this.events = response.events;

            refresher.complete();

        }, (error: Error) => {

            this.is_error = true;

            refresher.complete();
        });
    }

    goToDetails(event: any): void {

        this.navCtrl.push('EventDetailPage', {
            event: event
        });
    }

    create(): void {

        let profileModal = this.modalCtrl.create('EventCreatePage');
        profileModal.onDidDismiss((data: any) => {
            
            if (data && (data.event && data.event.posted)) {

                this.events.unshift(data.event);
            }
        });
        profileModal.present();
    }
}
